<#This script is to set advacned NIC settings according to ServiceNow KB0002014. 
You can reset back to default with 'Reset-NetAdapterAdvancedProperty -displayname * -norestart'
Please review the settings before running
Author: Bryan Montalban 1/31/17
#>


write-host "Current NIC settings will be displayed, then the settings will be updated.
The final output are the updated settings"  -ForegroundColor "Green"

Get-NetAdapterAdvancedProperty -displayname "*checksum offload*", "jumbo*", "large send*", "receive side*" | sort systemname, name, displayname | ft -groupby systemname -property name, displayname, displayvalue

$yes = New-Object System.Management.Automation.Host.ChoiceDescription "&Yes",""
$no = New-Object System.Management.Automation.Host.ChoiceDescription "&No",""
$choices = [System.Management.Automation.Host.ChoiceDescription[]]($yes,$no)
$caption = "Warning!"
$message = "Do you want modify Jumbo Packets for an iSCSI interface?"
$result = $Host.UI.PromptForChoice($caption,$message,$choices,0)


#This block will update the iSCSI interface Jumbo Packet settings.

if($result -eq 0){
Write-host "Please enter the name of the iSCSI adapter(s) to set for Jumbo Frames (fiscsiormat input as leading characters in the adapter name, ie 'iSCSI*')." -ForegroundColor "Green"
$iscsi = Read-Host "Adapter name"
Set-NetAdapterAdvancedProperty -DisplayName ”IPv4 Checksum Offload” -DisplayValue “Disabled” -NoRestart -ea Silentlycontinue
Set-NetAdapterAdvancedProperty -DisplayName “Large Send Offload V2 (IPv4)” -DisplayValue “Disabled” -NoRestart -ea Silentlycontinue
Set-NetAdapterAdvancedProperty -DisplayName “Large Send Offload V2 (IPv6)” -DisplayValue “Disabled” –NoRestart -ea Silentlycontinue
Set-NetAdapterAdvancedProperty -DisplayName “Large Send Offload Version 2 (IPv4)” -DisplayValue “Disabled” -NoRestart -ea Silentlycontinue
Set-NetAdapterAdvancedProperty -DisplayName “Large Send Offload Version 2 (IPv6)” -DisplayValue “Disabled” –NoRestart -ea Silentlycontinue
Set-NetAdapterAdvancedProperty -DisplayName ”TCP Checksum Offload (IPv4)” -DisplayValue “Disabled” –NoRestart -ea Silentlycontinue
Set-NetAdapterAdvancedProperty -DisplayName ”TCP Checksum Offload (IPv6)” -DisplayValue “Disabled” -NoRestart -ea Silentlycontinue
Set-NetAdapterAdvancedProperty -DisplayName ”UDP Checksum Offload (IPv4)” -DisplayValue “Disabled” –NoRestart -ea Silentlycontinue
Set-NetAdapterAdvancedProperty -DisplayName ”UDP Checksum Offload (IPv6)” -DisplayValue “Disabled” -NoRestart -ea Silentlycontinue
Set-NetAdapterAdvancedProperty -DisplayName ”Receive Side Scaling” -DisplayValue “Enabled” -NoRestart -ea Silentlycontinue
Set-NetAdapterAdvancedProperty $iscsi -DisplayName "Jumbo Packet" -DisplayValue "Jumbo 9000" -NoRestart -ea SilentlyContinue
}

#This block will not update Jumbo Packets for any interfaces

if($result -eq 1){
Set-NetAdapterAdvancedProperty -DisplayName ”IPv4 Checksum Offload” -DisplayValue “Disabled” -NoRestart -ea Silentlycontinue
Set-NetAdapterAdvancedProperty -DisplayName “Large Send Offload V2 (IPv4)” -DisplayValue “Disabled” -NoRestart -ea Silentlycontinue
Set-NetAdapterAdvancedProperty -DisplayName “Large Send Offload V2 (IPv6)” -DisplayValue “Disabled” –NoRestart -ea Silentlycontinue
Set-NetAdapterAdvancedProperty -DisplayName “Large Send Offload Version 2 (IPv4)” -DisplayValue “Disabled” -NoRestart -ea Silentlycontinue
Set-NetAdapterAdvancedProperty -DisplayName “Large Send Offload Version 2 (IPv6)” -DisplayValue “Disabled” –NoRestart -ea Silentlycontinue
Set-NetAdapterAdvancedProperty -DisplayName ”TCP Checksum Offload (IPv4)” -DisplayValue “Disabled” –NoRestart -ea Silentlycontinue
Set-NetAdapterAdvancedProperty -DisplayName ”TCP Checksum Offload (IPv6)” -DisplayValue “Disabled” -NoRestart -ea Silentlycontinue
Set-NetAdapterAdvancedProperty -DisplayName ”UDP Checksum Offload (IPv4)” -DisplayValue “Disabled” –NoRestart -ea Silentlycontinue
Set-NetAdapterAdvancedProperty -DisplayName ”UDP Checksum Offload (IPv6)” -DisplayValue “Disabled” -NoRestart -ea Silentlycontinue
Set-NetAdapterAdvancedProperty -DisplayName ”Receive Side Scaling” -DisplayValue “Enabled” -NoRestart -ea Silentlycontinue
}

write-host "NIC settings have been updated. Please verify changes were successfull against the table above." -ForegroundColor "Green"

Get-NetAdapterAdvancedProperty -displayname "*checksum offload*", "jumbo*", "large send*", "receive side*" | sort systemname, name, displayname | ft -groupby systemname -property name, displayname, displayvalue